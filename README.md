Expire Nodequeues
=================

This module extends default functionality of the Cache Expiration module and 
provides actions for Nodequeues. Nodequeue Cache Expiration module allows you 
to invalidate cache of some pages when a nodequeue gets updated, or when nodes 
are added/removed from a nodequeue.
