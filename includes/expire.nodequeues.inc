<?php

/**
 * @file
 * Provides class that expires nodequeues.
 */

/**
 * Class ExpireNodequeues.
 */
class ExpireNodequeues implements ExpireInterface {

  /**
   * Executes expiration actions for nodequeues.
   *
   * @param object $display
   *   The nodequeue object.
   * @param int $action
   *   Action that has been executed.
   * @param bool $skip_action_check
   *   Shows whether should we check executed action or just expire nodequeue.
   */
  public function expire($display, $action, $skip_action_check = FALSE) {
    $enabled_actions = variable_get('expire_nodequeues_actions', array());
    $enabled_actions = array_filter($enabled_actions);

    // Stop further expiration if executed action is not selected by admin.
    if (!in_array($action, $enabled_actions) && !$skip_action_check) {
      return;
    }

    $expire_urls = array();

    // Expire front page.
    $expire_front_page = variable_get('expire_nodequeues_front_page', EXPIRE_NODEQUEUES_FRONT_PAGE);
    if ($expire_front_page) {
      $expire_urls = ExpireAPI::getFrontPageUrls();
    }

    // Expire node page.
    $expire_node_page = variable_get('expire_nodequeues_node_page', EXPIRE_NODEQUEUES_NODE_PAGE);
    if ($expire_node_page && !empty($display->nid)) {
      $expire_urls['node-' . $display->nid] = 'node/' . $display->nid;
    }

    // Expire custom pages.
    $expire_custom = variable_get('expire_nodequeues_custom', EXPIRE_NODEQUEUES_CUSTOM);
    if ($expire_custom) {
      $pages = variable_get('expire_nodequeues_custom_pages');

      $nodequeue = !empty($display->qid) ? nodequeue_load($display->qid) : NULL;
      $subqueue = !empty($display->sqid) ? nodequeue_load_subqueue($display->sqid) : NULL;
      $node = !empty($display->nid) ? node_load($display->nid) : NULL;

      $urls = ExpireAPI::expireCustomPages($pages, array(
        'nodequeue' => $nodequeue,
        'subqueue' => $subqueue,
        'node' => $node,
      ));
      $expire_urls = array_merge($expire_urls, $urls);
    }

    // Flush page cache for expired urls.
    ExpireAPI::executeExpiration($expire_urls, 'nodequeues', $display);
  }

}
