<?php

/**
 * @file
 * Admin settings.
 */

/**
 * Nodequeues expiration settings.
 */
function expire_nodequeues_admin_settings_form_elements(&$form, &$form_state, $form_id) {
  $form['tabs']['nodequeues'] = array(
    '#type' => 'fieldset',
    '#title' => t('Nodequeues expiration'),
    '#group' => 'tabs',
    '#weight' => 8,
  );

  $form['tabs']['nodequeues']['actions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Nodequeues actions'),
  );

  $form['tabs']['nodequeues']['actions']['expire_nodequeues_actions'] = array(
    '#type' => 'checkboxes',
    '#description' => t('Page cache for nodequeues will be flushed after selected actions.'),
    '#options' => array(
      EXPIRE_NODEQUEUES_ADD_NODE => t('Node added in a nodequeue'),
      EXPIRE_NODEQUEUES_REMOVE_NODE => t('Node removed from a nodequeue'),
      EXPIRE_NODEQUEUES_CHANGE_ORDER => t('Node order changed in a nodequeue'),
      EXPIRE_NODEQUEUES_NODE_UPDATE => t('Node appearing in a nodequeue is updated'),
    ),
    '#default_value' => variable_get('expire_nodequeues_actions', array()),
  );

  $form['tabs']['nodequeues']['expire'] = array(
    '#type' => 'fieldset',
    '#title' => t('What URLs should be expired when nodequeues action is triggered?'),
  );

  $form['tabs']['nodequeues']['expire']['expire_nodequeues_front_page'] = array(
    '#type' => 'checkbox',
    '#title' => t('Front page'),
    '#description' => t('Expire url of the site front page'),
    '#default_value' => variable_get('expire_nodequeues_front_page', EXPIRE_NODEQUEUES_FRONT_PAGE),
  );

  $form['tabs']['nodequeues']['expire']['expire_nodequeues_node_page'] = array(
    '#type' => 'checkbox',
    '#title' => t('Node page'),
    '#description' => t('Expire url of the added/removed node page.'),
    '#default_value' => variable_get('expire_nodequeues_node_page', EXPIRE_NODEQUEUES_NODE_PAGE),
  );

  $form['tabs']['nodequeues']['expire']['expire_nodequeues_custom'] = array(
    '#type' => 'checkbox',
    '#title' => t('Custom pages'),
    '#description' => t('Expire user-defined custom urls.'),
    '#default_value' => variable_get('expire_nodequeues_custom', EXPIRE_NODEQUEUES_CUSTOM),
  );

  // @codingStandardsIgnoreStart
  $form['tabs']['nodequeues']['expire']['expire_nodequeues_custom_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Enter custom URLs'),
    '#description' => t('Enter one relative URL per line. It can be the path of a node (e.g. !example1) or of any alias (e.g. !example2). However, it has to be the final URL, not a redirect (use the !link1 and !link2 modules).', array(
        '!example1' => '<strong>user/[user:uid]</strong>',
        '!example2' => '<strong>my/path</strong>',
        '!link1' => l('Global Redirect', 'https://drupal.org/project/globalredirect'),
        '!link2' => l('Redirect', 'https://drupal.org/project/redirect')
      )) . '<br/>'
      . t('If you want to match a path with any ending, add "|wildcard" to the end of the line (see !link1 for details). Example: !example1 will match !example1a, but also !example1b, !example1c, etc.', array(
        '!link1' => l('function cache_clear_all', 'https://api.drupal.org/api/drupal/includes%21cache.inc/function/cache_clear_all/7'),
        '!example1' => '<strong>my/path|wildcard</strong>',
        '!example1a' => '<strong>my/path</strong>',
        '!example1b' => '<strong>my/path/one</strong>',
        '!example1c' => '<strong>my/path/two</strong>'
      )) . '<br/>'
      . t('You may use tokens here.'),
    '#states' => array(
      'visible' => array(
        ':input[name="expire_nodequeues_custom"]' => array('checked' => TRUE),
      ),
    ),
    '#default_value' => variable_get('expire_nodequeues_custom_pages'),
  );
  // @codingStandardsIgnoreEnd
}
